
import requests

from .models import UserToken, Appliance

URL = "https://api.delta.electrolux.com/api/"

class WellbeingApi:
  def __init__(self, client_secret: str, refresh_token: str = None):
    self._client_secret = client_secret
    self._refresh_token = refresh_token

  def init_client_token(self):
    resp = requests.post(URL + "Clients/Wellbeing", json={
      "ClientSecret": self._client_secret
    })
    self._client_token = resp.json()["accessToken"]

  def login(self, username: str, password: str) -> str:
    headers = {"Authorization": "Bearer " + self._client_token}
    resp = requests.post(URL + "Users/Login", json={
      "Username": username, "Password": password
    }, headers=headers)
    self._user_token = UserToken(resp.json())
    self._refresh_token = self._user_token.refresh_token
    return self._refresh_token

  def refresh_token(self) -> str:
    resp = requests.post(URL + "/Users/RefreshToken", json={
      "ClientId": "Wellbeing",
      "ClientSecret": self._client_secret,
      "RefreshToken": self._refresh_token
    })
    self._user_token = UserToken(resp.json())
    self._refresh_token = self._user_token.refresh_token
    return self._refresh_token

  def get_appliances(self):
    self._refresh_token_maybe()
    headers = {"Authorization": "Bearer " + self._user_token.access_token}
    resp = requests.get(URL + "/Domains/Appliances", headers=headers)
    body = resp.json()
    return [Appliance(x) for x in body]

  def get_appliance(self, appliance: Appliance):
    self._refresh_token_maybe()
    headers = {"Authorization": "Bearer " + self._user_token.access_token}
    resp = requests.get(URL + "/Appliances/" + appliance.pnc_id, headers=headers)
    return resp.json()

  def change_settings(self, appliance: Appliance, json: dict):
    self._refresh_token_maybe()
    headers = {"Authorization": "Bearer " + self._user_token.access_token}
    requests.put(URL + "/Appliances/" + appliance.pnc_id, json=json, headers=headers)

  def send_command(self, appliance: Appliance, json: dict):
    self._refresh_token_maybe()
    headers = {"Authorization": "Bearer " + self._user_token.access_token}
    requests.put(URL + "/Appliances/" + appliance.pnc_id + "/Commands", json=json, headers=headers)

  def _refresh_token_maybe(self):
    if self._user_token.is_expired():
      self.refresh_token()
