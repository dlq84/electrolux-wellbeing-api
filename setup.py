from setuptools import setup, find_packages  # noqa: H301

NAME = "electrolux_wellbeing"
VERSION = "0.0.1"
# To install the library, run the following
#
# python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

REQUIRES = []

setup(
    name=NAME,
    version=VERSION,
    description="Electrolux Wellbeing Api",
    author_email="",
    url="",
    keywords=["Swagger", "Electrolux Wellbeing Api"],
    install_requires=REQUIRES,
    packages=find_packages(),
    include_package_data=True,
    long_description="""\
    &lt;h3&gt;A reverse engineered (incomplete) specification of the Electrolux Wellbeing (A.K.A delta) Api.&lt;/h3&gt; &lt;p&gt;This version of the spec only contains calls for authentication and controlling the Pure i9 line of robot vacuum cleaners.&lt;/p&gt; &lt;p&gt;&lt;b&gt;Repo:&lt;/b&gt; &lt;a href&#x3D;\&quot;https://gitlab.com/dlq84/py-electrolux-wellbeing\&quot;&gt;https://gitlab.com/dlq84/py-electrolux-wellbeing&lt;/a&gt;&lt;/p&gt;   # noqa: E501
    """
)
