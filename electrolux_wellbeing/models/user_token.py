import datetime
MINUTE = 60

class UserToken:
  def __init__(self, body) -> None:
    self.access_token = body['accessToken']
    self.refresh_token = body['refreshToken']
    self.token_type = body['tokenType']
    self.expires_in = body['expiresIn']
    # set expiration datetime
    now = datetime.datetime.now()
    # set two minutes earlier just to be safe
    self._exp_time = now + datetime.timedelta(seconds=self.expires_in - MINUTE * 2)

  def is_expired(self) -> bool:
    now = datetime.datetime.now()
    return (self._exp_time - now).total_seconds() < 0
