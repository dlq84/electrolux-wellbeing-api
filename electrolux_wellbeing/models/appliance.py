
class Appliance:
  def __init__(self, body) -> None:
    self.appliance_name = body['applianceName']
    self.created = body['created']
    self.id = body['id']
    self.model_name = body['modelName']
    self.time_zone_standard_name = body['timeZoneStandardName']
    self.pnc_id = body['pncId']
    self.domain_id = body['domainId']
